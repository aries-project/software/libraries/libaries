#define CATCH_CONFIG_MAIN // This tells Catch to provide a main() - only do this in one cpp file
#include <aries/firmware/intel_hex.hpp>
#include <catch2/catch.hpp>
#include <cstdint>
#include <vector>

using namespace aries::firmware;

SCENARIO("Create IntelHex object", "[intel_hex_tests]")
{
    GIVEN("New object is created")
    {
        auto ihex = new IntelHex();

        REQUIRE(ihex != nullptr);

        WHEN("Object is not modified")
        {
            THEN("Default word_size is set") { REQUIRE(ihex->getWordSize() == 16); }
        }

        WHEN("Object is deleted")
        {
            THEN("No exceptions are thrown")
            {
                REQUIRE_NOTHROW([&]() { delete ihex; }());
            }
        }
    }
}

SCENARIO("Load content from file", "[intel_hex_tests]")
{
    GIVEN("New object is created")
    {
        IntelHex ihex;

        bool ret = ihex.loadFromFile(TEST_DATA_DIR "/files/example.hex");

        REQUIRE(ret);
        REQUIRE(ihex.getRecords().size() == 5);

        auto& recs = ihex.getRecords();

        REQUIRE(recs[0].rec_type == IntelHexRecordType::DATA);
        REQUIRE(recs[0].address == 0x0100);
        REQUIRE(recs[0].data ==
                std::vector<uint8_t>{
                    0x21, 0x46, 0x01, 0x36, 0x01, 0x21, 0x47, 0x01, 0x36, 0x00, 0x7E, 0xFE, 0x09, 0xD2, 0x19, 0x01 });
        REQUIRE(recs[0].checksum == 0x40);

        REQUIRE(recs[1].rec_type == IntelHexRecordType::DATA);
        REQUIRE(recs[1].address == 0x0110);
        REQUIRE(recs[1].data ==
                std::vector<uint8_t>{
                    0x21, 0x46, 0x01, 0x7E, 0x17, 0xC2, 0x00, 0x01, 0xFF, 0x5F, 0x16, 0x00, 0x21, 0x48, 0x01, 0x19 });
        REQUIRE(recs[1].checksum == 0x28);

        REQUIRE(recs[2].rec_type == IntelHexRecordType::DATA);
        REQUIRE(recs[2].address == 0x0120);
        REQUIRE(recs[2].data ==
                std::vector<uint8_t>{
                    0x19, 0x4E, 0x79, 0x23, 0x46, 0x23, 0x96, 0x57, 0x78, 0x23, 0x9E, 0xDA, 0x3F, 0x01, 0xB2, 0xCA });
        REQUIRE(recs[2].checksum == 0xA7);

        REQUIRE(recs[3].rec_type == IntelHexRecordType::DATA);
        REQUIRE(recs[3].address == 0x0130);
        REQUIRE(recs[3].data ==
                std::vector<uint8_t>{
                    0x3F, 0x01, 0x56, 0x70, 0x2B, 0x5E, 0x71, 0x2B, 0x72, 0x2B, 0x73, 0x21, 0x46, 0x01, 0x34, 0x21 });
        REQUIRE(recs[3].checksum == 0xC7);

        REQUIRE(recs[4].rec_type == IntelHexRecordType::END_OF_FILE);
        REQUIRE(recs[4].address == 0x0000);
        REQUIRE(recs[4].data == std::vector<uint8_t>{});
        REQUIRE(recs[4].checksum == 0xFF);
    }
}