#include <aries/firmware/base.hpp>
#include <iostream>
#include <serialxx/thread_safe.hpp>
#include <stdio.h>

using namespace aries::firmware;
using namespace serialxx::thread_safe;

int main(int argc, char* argv[])
{
    Serial ser;
    ser.setPortName("/dev/ttyUSB0");
    ser.setBaudRate(1000000);
    ser.setTimeout(200);
    if (!ser.open()) {
        std::cout << "Failed to open serial port\n";
        return -1;
    }

    Base base(ser);
    base.getAlive(0x08);

    // std::vector<uint8_t> data = { 0x80, 0x00, 0x01, 0x08, 0xc8 };
    std::vector<uint8_t> data = { 0x08, 0x00, 0x01, 0x08, 0x61 };
    ser.write(data);

    std::vector<uint8_t> resp;
    auto len = ser.read(resp, 5, 200);

    std::cout << "Received: " << std::dec << len << std::endl;
    for (auto r : resp) {
        std::cout << "0x" << std::hex << (int)r << std::endl;
    }

    return 0;
}