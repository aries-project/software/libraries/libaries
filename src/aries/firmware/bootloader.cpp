/**
 * libaries
 * Copyright (C) 2021  aries-platform / libraries
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @file bootloader.cpp
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 *
 */
#include "aries/firmware/bootloader.hpp"

// section:manual_includes
#include <aries/firmware/intel_hex.hpp>
#include <aries/firmware/std.hpp>
#include <iostream>
// endsection:manual_includes

namespace aries {
namespace firmware {

    Bootloader::Bootloader(serialxx::thread_safe::Serial& serial)
      : aries::firmware::Base(serial)
    {}

    std::vector<uint8_t> Bootloader::getSignature(uint8_t address)
    {
        // section:get_signature_pcvrzbkvn7l6nijiwbixtcanra
        /* add your implementation here */
        auto resp = send(address, { CMD_V2 }, 4);
        if (resp.valid) {
            return std::vector<uint8_t>(resp.payload.begin() + 1, resp.payload.end());
        }
        return { 0x00, 0x00, 0x00 };
        // endsection:get_signature_pcvrzbkvn7l6nijiwbixtcanra
    }

    bool Bootloader::checkSignature(uint8_t address, const std::vector<uint8_t>& signature)
    {
        // section:check_signature_tidrlnrrb7u4fspsvtkss7yuci
        auto mcu_sig = getSignature(address);
        if (mcu_sig.size() != signature.size())
            return false;

        for (int i = 0; i < mcu_sig.size(); i++) {
            if (mcu_sig[i] != signature[i])
                return false;
        }

        return true;
        // endsection:check_signature_tidrlnrrb7u4fspsvtkss7yuci
    }

    bool Bootloader::setBusId(uint8_t address, uint8_t new_address)
    {
        // section:set_bus_id_cxxy2wvyhtktnce4c7wlqaylqm
        auto resp = send(address, { CMD_W1 }, 1);
        return resp.valid;
        // endsection:set_bus_id_cxxy2wvyhtktnce4c7wlqaylqm
    }

    bool Bootloader::startApplication(uint8_t address)
    {
        // section:start_application_rk3q3fkff4q76ybdp37tafzbju
        auto resp = send(address, { CMD_L1 }, 1);
        return resp.valid;
        // endsection:start_application_rk3q3fkff4q76ybdp37tafzbju
    }

    bool Bootloader::setLoadAddress(uint8_t address, uint16_t load_address)
    {
        // section:set_load_address_uk2jgpr2hfww7ndtnwvxztzfyy
        uint8_t addr_low  = load_address & 0xff;
        uint8_t addr_high = (load_address >> 8) & 0xff;

        auto resp = send(address,
                         {
                             CMD_L0,
                             addr_low,
                             addr_high,
                         },
                         1);
        return resp.valid;
        // endsection:set_load_address_uk2jgpr2hfww7ndtnwvxztzfyy
    }

    bool Bootloader::writePage(uint8_t address, const std::vector<uint8_t>& data, MemoryType memory_type)
    {
        // section:write_page_2uzh4im6igbkq7i4t2xmpoyzbq
        size_t length    = data.size();
        uint8_t len_low  = length & 0xff;
        uint8_t len_high = (length >> 8) & 0xff;
        uint8_t mtype    = memory_type == MemoryType::FLASH ? 'F' : 'E';

        std::vector<uint8_t> cmd = {
            CMD_W0,
            len_high,
            len_low,
            mtype,
        };
        cmd.reserve(4 + length);

        for (size_t i = 0; i < length; i++) {
            cmd.push_back(data[i]);
        }

        auto resp = send(address, cmd, 1);

        return resp.valid;
        // endsection:write_page_2uzh4im6igbkq7i4t2xmpoyzbq
    }

    bool Bootloader::readPage(uint8_t address, std::vector<uint8_t>& data, MemoryType memory_type)
    {
        // section:read_page_xy6opgumeemsobb2pey3u2zyr4
        size_t length    = data.size();
        uint8_t len_low  = length & 0xff;
        uint8_t len_high = (length >> 8) & 0xff;
        uint8_t mtype    = memory_type == MemoryType::FLASH ? 'F' : 'E';

        auto resp = send(address,
                         {
                             CMD_L0,
                             len_high,
                             len_low,
                             mtype,
                         },
                         length + 1);

        for (size_t i = 0; i < length; i++) {
            data[i] = resp.payload[i + 1];
        }

        return resp.valid;
        // endsection:read_page_xy6opgumeemsobb2pey3u2zyr4
    }

    bool Bootloader::uploadFirmware(uint8_t address, const std::string& filename, const std::string& mcu)
    {
        // section:upload_firmware_ad6mmy34larmn4hrz5wzuaa3fa
        unsigned page_size                 = 64; // in words
        std::vector<uint8_t> mcu_signature = { 0x1e, 0x95, 0x16 };

        IntelHex ihex(16);
        if (!ihex.loadFromFile(filename))
            return false;

        // Get MCU config

        // Check signature
        if (!checkSignature(address, mcu_signature)) {
            std::cout << "Failed to verify Signature\n";
            return false;
        } else {
            std::cout << "Signature: OK\n";
        }

        auto pages = ihex.getPages(page_size);

        for (auto& page : pages) {
            if (setLoadAddress(address, page.address)) {
                std::cout << "LoadAddress: 0x" << std::hex << page.address << std::endl;

                if (!writePage(address, page.data, MemoryType::FLASH)) {
                    if (m_debug) {
                        std::cout << "Failed to WritePage @ 0x" << std::hex << page.address << std::endl;
                    }
                    return false;
                } else {
                    if (m_debug) {
                        std::cout << "Page write success 0x" << std::hex << page.address << std::endl;
                    }
                }
            } else {
                std::cout << "Failed to set LoadAddress @ 0x" << std::hex << page.address << std::endl;
                return false;
            }
        }

        return true;
        // endsection:upload_firmware_ad6mmy34larmn4hrz5wzuaa3fa
    }

    bool Bootloader::downloadFirmware(uint8_t address, const std::string& filename, const std::string& mcu)
    {
        // section:download_firmware_erqbyzr6xtoiod5vbngfhbabda

        // flash_size 32768 bytes, 16K words
        // page_size 128,   64 words
        // page_count 256

        unsigned page_size  = 128;
        unsigned page_count = 256;

        for (unsigned page_idx = 0; page_idx < page_count; page_idx++) {
            unsigned start_addr = page_idx * (page_size >> 1);
            if (setLoadAddress(address, start_addr)) {
                std::cout << "LA: " << std::hex << start_addr << std::endl;
                // TODO: readPage
            } else {
                return false;
            }
        }

        return true;
        // endsection:download_firmware_erqbyzr6xtoiod5vbngfhbabda
    }

    bool Bootloader::uploadEeprom(uint8_t address, const std::string& filename, const std::string& mcu)
    {
        // section:upload_eeprom_xhsy3nvkg5s3hufs36zubnofom
        /* add your implementation here */
        // endsection:upload_eeprom_xhsy3nvkg5s3hufs36zubnofom
    }

    bool Bootloader::downloadEeprom(uint8_t address, const std::string& filename, const std::string& mcu)
    {
        // section:download_eeprom_oxechju2ox3suerwnoirki7jdy
        /* add your implementation here */
        // endsection:download_eeprom_oxechju2ox3suerwnoirki7jdy
    }

} // namespace firmware

} // namespace aries

// section:manual_code
// endsection:manual_code