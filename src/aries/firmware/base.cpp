/**
 * libaries
 * Copyright (C) 2021  aries-platform / libraries
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @file base.cpp
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 *
 */
#include "aries/firmware/base.hpp"

// section:manual_includes
#include <aries/firmware/crc8.hpp>
#include <aries/firmware/exceptions.hpp>
#include <aries/firmware/std.hpp>
#include <iostream>
// endsection:manual_includes

namespace aries {
namespace firmware {

    Base::Base(serialxx::thread_safe::Serial& serial)
      : m_serial(serial)
    {}

    unsigned Base::write(uint8_t address, const std::vector<uint8_t>& payload)
    {
        // section:write_7iyxdnyc4hfg6pzlol77z44hsy
        unsigned data_len = payload.size();
        uint8_t len_low   = data_len & 0xff;
        uint8_t len_high  = (data_len >> 8) & 0xff;
        uint8_t crc       = CRC8_start();

        crc = CRC8_add_byte(address, crc);
        crc = CRC8_add_byte(len_high, crc);
        crc = CRC8_add_byte(len_low, crc);
        crc = CRC8_add_buff(payload.data(), payload.size(), crc);
        crc = CRC8_finish(crc);

        std::vector<uint8_t> data;
        data.reserve(ARIES_ADDR_FIELD_BYTES + ARIES_LENGTH_FIELD_BYTES + payload.size() + ARIES_CRC_FIELD_BYTES);

        data.push_back(address);
        data.push_back(len_high);
        data.push_back(len_low);
        for (auto& b : payload) {
            data.push_back(b);
        }
        data.push_back(crc);

        // for (auto& c : data) {
        //     std::cout << "0x" << std::hex << (int)c << std::endl;
        // }

        size_t written = 0;
        written        = m_serial.write(data);
        // written += m_serial.writeByte(address);
        // written += m_serial.writeByte(len_low);
        // written += m_serial.writeByte(len_high);
        // written += m_serial.write(payload);
        // written += m_serial.writeByte(crc);

        return written;
        // endsection:write_7iyxdnyc4hfg6pzlol77z44hsy
    }

    MessageResponse Base::send(uint8_t address, const std::vector<uint8_t>& payload, unsigned max_length)
    {
        // section:send_w2mwhjwl36gupfxg6fdwhqvdju
        write(address, payload);
        return recv(max_length);
        // endsection:send_w2mwhjwl36gupfxg6fdwhqvdju
    }

    MessageResponse Base::recv(unsigned max_length)
    {
        // section:recv_xlrnigomotkmwsahorsf7qlhgm
        unsigned minimum_byte_count =
            ARIES_ADDR_FIELD_BYTES + ARIES_LENGTH_FIELD_BYTES + ARIES_MIN_RESP_BYTES + ARIES_CRC_FIELD_BYTES;

        if (max_length == 0)
            max_length = ARIES_MAX_PAYLOAD_BYTES;

        unsigned expected_byte_count = max_length - ARIES_MIN_RESP_BYTES + minimum_byte_count;

        MessageResponse resp;
        resp.valid   = false;
        resp.address = 0x00;

        std::vector<uint8_t> wire_data;
        try {
            size_t len = m_serial.read(wire_data, expected_byte_count, ARIES_MSG_TIMEOUT_MS);
            if (len < minimum_byte_count) {
                throw aries::ProtocolError("Minimum bytes required are " + std::to_string(minimum_byte_count) +
                                           " but " + std::to_string(len) + " received.");
            }

            if (m_debug) {
                std::cout << "received: " << len << " bytes (expected: " << expected_byte_count << ")\n";
                for (size_t i = 0; i < len; i++) {
                    std::cout << "0x" << std::hex << (int)wire_data[i] << ", ";
                }
                std::cout << "\n";
            }

            uint8_t crc = CRC8_calculate(wire_data.data(), len - 1);
            if (crc != wire_data[len - 1]) {
                throw aries::CrcError("Message checksum failure.");
            } else {
                resp.address = wire_data[0];
            }
            resp.address = wire_data[0];
        } catch (serialxx::ReadTimeout& e) {
            throw aries::TimeoutError("Serial read timeout.");
        } catch (std::exception& e) {
            throw aries::IOError(e.what());
        }

        resp.payload = std::vector<uint8_t>(wire_data.begin() + ARIES_ADDR_FIELD_BYTES + ARIES_LENGTH_FIELD_BYTES,
                                            wire_data.end() - ARIES_CRC_FIELD_BYTES);

        resp.valid = resp.payload[0] == RESP_OK;

        return resp;
        // endsection:recv_xlrnigomotkmwsahorsf7qlhgm
    }

    bool Base::getAlive(uint8_t address)
    {
        // section:get_alive_w2iygdfq5shiqxjto6jqbrp5nu
        auto resp = send(address, { CMD_A0 }, 1);
        return resp.valid;
        // endsection:get_alive_w2iygdfq5shiqxjto6jqbrp5nu
    }

    FwVersion Base::getFwVersion(uint8_t address)
    {
        // section:get_fw_version_cmthv6dit2fxnssouuvmyuzn2a
        /**
         * [0] - RESP_OK
         * [1] - MAJOR
         * [2] - MINOR
         * [3] - PATCH
         */
        auto resp = send(address, { CMD_V0 }, 4);
        FwVersion ver{ 0, 0, 0 };

        if (resp.valid && resp.payload.size() == 4) {
            ver.major = resp.payload[1];
            ver.minor = resp.payload[2];
            ver.patch = resp.payload[3];
        }

        return ver;
        // endsection:get_fw_version_cmthv6dit2fxnssouuvmyuzn2a
    }

    std::string Base::getFwName(uint8_t address)
    {
        // section:get_fw_name_54bkqvpat3e7d35twxc72qxuuq
        /**
         * [0]   - RESP_OK
         * [1]   - First char
         * [...]
         * [15]  - Last char
         */
        auto resp = send(address, { CMD_V1 }, 16);

        if (resp.valid) {
            //
            char buff[16];
            char* bit = buff;
            for (auto it = resp.payload.begin() + 1; it != resp.payload.end(); ++it) {
                *bit++ = static_cast<char>(*it);
            }
            *bit = '\0';
            return buff;
        }

        return "";
        // endsection:get_fw_name_54bkqvpat3e7d35twxc72qxuuq
    }

    void Base::setDebug(bool value) { m_debug = value; }

    bool Base::getDebug() const { return m_debug; }

} // namespace firmware

} // namespace aries

// section:manual_code
// endsection:manual_code