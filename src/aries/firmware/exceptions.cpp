/**
 * libaries
 * Copyright (C) 2021  aries-platform / libraries
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @file exceptions.cpp
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 *
 */
#include "aries/firmware/exceptions.hpp"

// section:manual_includes
// endsection:manual_includes

namespace aries {

CrcError::CrcError(const std::string& what)
  : std::runtime_error(what)
{}

ProtocolError::ProtocolError(const std::string& what)
  : std::runtime_error(what)
{}

TimeoutError::TimeoutError(const std::string& what)
  : std::runtime_error(what)
{}

IOError::IOError(const std::string& what)
  : std::runtime_error(what)
{}

} // namespace aries

// section:manual_code
// endsection:manual_code