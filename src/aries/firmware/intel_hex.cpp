/**
 * libaries
 * Copyright (C) 2021  aries-platform / libraries
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @file intel_hex.cpp
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 *
 */
#include "aries/firmware/intel_hex.hpp"

// section:manual_includes
#include <cmath>
#include <exception>
#include <fstream>
#include <iostream>
#include <string>
// endsection:manual_includes

namespace aries {
namespace firmware {

    IntelHex::IntelHex(unsigned word_size)
      : m_word_size(word_size)
    {
        // section:<create>_d4bwbtulso2qywqojbwtnfwkmu
        /* add your implementation here */
        // endsection:<create>_d4bwbtulso2qywqojbwtnfwkmu
    }

    bool IntelHex::parseRecord(const std::string& line, IntelHexRecord& record)
    {
        // section:parse_record_bt3iyn7xir347ubmutf3tqnenu

        /*

        :    - colon        [0]
        00   - byte count   [1..2]
        0000 - address      [3..6]
        00   - record type  [7..8]
        ...  - data         [..]
        00   - checksum     [9..10]

        min_size = 1 + 2 + 4 + 2 + 2 = 11

        */

        if (line.size() < 11)
            return false;

        if (line[0] != ':')
            return false;

        unsigned pos = 1;

        auto byte_count_hex = line.substr(pos, 2);
        int byte_count      = std::stoi(byte_count_hex, nullptr, 16);
        pos += 2;

        auto address_hex = line.substr(pos, 4);
        record.address   = std::stoi(address_hex, nullptr, 16);
        pos += 4;

        auto rec_type_hex = line.substr(pos, 2);
        record.rec_type   = static_cast<IntelHexRecordType>(std::stoi(rec_type_hex, nullptr, 16));
        pos += 2;

        for (int i = 0; i < byte_count; i++) {
            auto data_byte_hex = line.substr(pos, 2);
            uint8_t data_byte  = std::stoi(data_byte_hex, nullptr, 16);
            record.data.push_back(data_byte);
            pos += 2;
        }

        auto checksum_hex = line.substr(pos, 2);
        record.checksum   = std::stoi(checksum_hex, nullptr, 16);

        return true;
        // endsection:parse_record_bt3iyn7xir347ubmutf3tqnenu
    }

    bool IntelHex::loadFromFile(const std::string& filename)
    {
        // section:load_from_file_kadbthsi4lzv7s4yddjffxtadm
        std::ifstream ihex;

        // std::cout << "filename: " << filename << std::endl;

        ihex.open(filename, std::ifstream::in);
        if (!ihex.is_open())
            return false;

        std::string line;
        while (std::getline(ihex, line)) {
            // std::cout << "L: " << line << std::endl;
            IntelHexRecord rec;
            if (parseRecord(line, rec)) {
                // std::cout << "  @" << std::hex << rec.address << " / " << std::dec << rec.data.size() << std::endl;
                if (rec.rec_type == IntelHexRecordType::DATA) {
                    m_byte_count += rec.data.size();
                }
                m_records.push_back(rec);
            } else {
                return false;
            }
        }

        return true;
        // endsection:load_from_file_kadbthsi4lzv7s4yddjffxtadm
    }

    unsigned IntelHex::getPageCount(unsigned page_word_size)
    {
        // section:get_page_count_vvpckygthwqh3lizhczr4ztsbm
        unsigned bytes_per_word = m_word_size / 8;
        unsigned bytes_per_page = page_word_size * bytes_per_word;
        return std::ceil((float)m_byte_count / bytes_per_page);
        // endsection:get_page_count_vvpckygthwqh3lizhczr4ztsbm
    }

    std::vector<IntelHexPage> IntelHex::getPages(unsigned page_word_size)
    {
        // section:get_pages_golqkvm2qpnv7yaxez6fncd5rm
        std::vector<IntelHexPage> pages;
        unsigned bytes_per_word = m_word_size / 8;
        unsigned bytes_per_page = bytes_per_word * page_word_size;

        IntelHexPage current_page{ 0, {} };
        current_page.data.reserve(bytes_per_page);

        for (auto& rec : m_records) {
            if (rec.rec_type == IntelHexRecordType::DATA) {
                for (unsigned i = 0; i < rec.data.size(); i++) {
                    current_page.data.push_back(rec.data[i]);

                    // If the page is full, add it to pages and create a new one
                    if (current_page.data.size() == bytes_per_page) {
                        // Reduce memory usage
                        current_page.data.shrink_to_fit();
                        pages.push_back(current_page);
                        // Create a new empty page at the next memory page location
                        current_page.address += page_word_size;
                        current_page.data.clear();
                        current_page.data.reserve(bytes_per_page);
                    }
                }
            } else if (rec.rec_type == IntelHexRecordType::END_OF_FILE) {
                // Ensure we add the last page as well
                if (current_page.data.size()) {
                    current_page.data.shrink_to_fit();
                    pages.push_back(current_page);
                }
            }
        }

        return pages;
        // endsection:get_pages_golqkvm2qpnv7yaxez6fncd5rm
    }

    unsigned IntelHex::getWordSize() const { return m_word_size; }

    const std::vector<IntelHexRecord>& IntelHex::getRecords() const { return m_records; }

    unsigned IntelHex::getByteCount() const { return m_byte_count; }

} // namespace firmware

} // namespace aries

// section:manual_code
// endsection:manual_code