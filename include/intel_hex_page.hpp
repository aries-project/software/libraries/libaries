/**
 * libaries
 * Copyright (C) 2021  aries-platform / libraries
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @file intel_hex_page.hpp
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 *
 */
#ifndef INTEL_HEX_PAGE_HPP
#define INTEL_HEX_PAGE_HPP

#include <cstdint>
#include <vector>
// section:manual_includes
// endsection:manual_includes

/**
 *
 */
struct IntelHexPage
{
    ///
    unsigned address;
    ///
    std::vector<uint8_t> data;
};

#endif /* INTEL_HEX_PAGE_HPP */