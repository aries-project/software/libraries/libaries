#ifndef ARIES_HPP
#define ARIES_HPP

#include <aries/firmware/base.hpp>
#include <aries/firmware/bootloader.hpp>
#include <aries/firmware/exceptions.hpp>
#include <aries/firmware/std.hpp>

#endif /* ARIES_HPP */