/**
 * libaries
 * Copyright (C) 2021  aries-platform / libraries
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @file bootloader.hpp
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 *
 */
#ifndef BOOTLOADER_HPP
#define BOOTLOADER_HPP

#include "base.hpp"
#include <cstdint>
#include <serialxx/serial.hpp>
#include <string>
#include <vector>
// section:manual_includes
// endsection:manual_includes

namespace aries {

namespace firmware {

    /**
     *
     */
    enum class MemoryType
    {
        FLASH,
        EEPROM,
    };

    /**
     *
     */
    class Bootloader : public Base
    {
        // section:manual_definitions
        // endsection:manual_definitions

      public:
        /**
         *
         * @param[out] serial
         */
        Bootloader(serialxx::thread_safe::Serial& serial);

        /**
         *
         * @param[in] address
         * @return
         */
        std::vector<uint8_t> getSignature(uint8_t address);

        /**
         *
         * @param[in] address
         * @param[in] signature
         * @return
         */
        bool checkSignature(uint8_t address, const std::vector<uint8_t>& signature);

        /**
         *
         * @param[in] address
         * @param[in] new_address
         * @return
         */
        bool setBusId(uint8_t address, uint8_t new_address);

        /**
         *
         * @param[in] address
         * @return
         */
        bool startApplication(uint8_t address);

        /**
         *
         * @param[in] address
         * @param[in] load_address
         * @return
         */
        bool setLoadAddress(uint8_t address, uint16_t load_address);

        /**
         *
         * @param[in] address
         * @param[in] data
         * @param[in] memory_type
         * @return
         */
        bool writePage(uint8_t address, const std::vector<uint8_t>& data, MemoryType memory_type);

        /**
         *
         * @param[in] address
         * @param[in] data
         * @param[in] memory_type
         * @return
         */
        bool readPage(uint8_t address, std::vector<uint8_t>& data, MemoryType memory_type);

        /**
         *
         * @param[in] address
         * @param[in] filename
         * @param[in] mcu
         * @return
         */
        bool uploadFirmware(uint8_t address, const std::string& filename, const std::string& mcu);

        /**
         *
         * @param[in] address
         * @param[in] filename
         * @param[in] mcu
         * @return
         */
        bool downloadFirmware(uint8_t address, const std::string& filename, const std::string& mcu);

        /**
         *
         * @param[in] address
         * @param[in] filename
         * @param[in] mcu
         * @return
         */
        bool uploadEeprom(uint8_t address, const std::string& filename, const std::string& mcu);

        /**
         *
         * @param[in] address
         * @param[in] filename
         * @param[in] mcu
         * @return
         */
        bool downloadEeprom(uint8_t address, const std::string& filename, const std::string& mcu);

      protected:
      private:
    };

} // namespace firmware

} // namespace aries

#endif /* BOOTLOADER_HPP */