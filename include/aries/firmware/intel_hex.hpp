/**
 * libaries
 * Copyright (C) 2021  aries-platform / libraries
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @file intel_hex.hpp
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 *
 */
#ifndef INTEL_HEX_HPP
#define INTEL_HEX_HPP

#include "intel_hex_page.hpp"
#include <cstdint>
#include <string>
#include <vector>
// section:manual_includes
// endsection:manual_includes

namespace aries {

namespace firmware {

    /**
     * Intel Hex file record type.
     */
    enum class IntelHexRecordType
    {
        DATA                     = 0,
        END_OF_FILE              = 1,
        EXTENDED_SEGMENT_ADDRESS = 2,
        START_SEGMENT_ADDRESS    = 3,
        EXTENDED_LINEAR_ADDRESS  = 4,
        START_LINEAR_ADDRESS     = 5,
    };

    /**
     * Intel Hex file record.
     */
    struct IntelHexRecord
    {
        ///
        IntelHexRecordType rec_type;
        ///
        uint32_t address;
        ///
        std::vector<uint8_t> data;
        ///
        uint8_t checksum;
    };

    /**
     * Intel Hex file format parser.
     */
    class IntelHex
    {
        // section:manual_definitions
        // endsection:manual_definitions

      public:
        /**
         *
         * @param[in] word_size
         */
        IntelHex(unsigned word_size = 16);

        /**
         *
         * @param[in] line
         * @param[out] record
         * @return
         */
        bool parseRecord(const std::string& line, IntelHexRecord& record);

        /**
         * Load iHex file content from a file.
         * @param[in] filename File path
         * @return
         */
        bool loadFromFile(const std::string& filename);

        /**
         *
         * @param[in] page_word_size
         * @return
         */
        unsigned getPageCount(unsigned page_word_size);

        /**
         *
         * @param[in] page_word_size
         * @return
         */
        std::vector<IntelHexPage> getPages(unsigned page_word_size);

        /**
         * Get word_size value
         * @return
         */
        unsigned getWordSize() const;

        /**
         * Get records value
         * @return
         */
        const std::vector<IntelHexRecord>& getRecords() const;

        /**
         * Get byte_count value
         * @return
         */
        unsigned getByteCount() const;

      protected:
      private:
        ///
        unsigned m_word_size = 16;
        ///
        std::vector<IntelHexRecord> m_records;
        ///
        unsigned m_byte_count = 0;
    };

} // namespace firmware

} // namespace aries

#endif /* INTEL_HEX_HPP */