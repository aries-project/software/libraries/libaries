#ifndef ARIES_CRC8_HPP
#define ARIES_CRC8_HPP

#include <cstdbool>
#include <cstdint>
#include <string>

uint8_t CRC8_calculate(const uint8_t* data_ptr, size_t length, uint8_t start_value8 = 0x00, bool is_first_call = true);

uint8_t CRC8_calculate(const std::string& data, uint8_t start_value8 = 0x00, bool is_first_call = true);

uint8_t CRC8_start();

uint8_t CRC8_add_byte(uint8_t data, uint8_t crc);

uint8_t CRC8_add_buff(const uint8_t* data, uint8_t len, uint8_t crc);

uint8_t CRC8_finish(uint8_t crc);

#endif /* ARIES_CRC8_HPP */