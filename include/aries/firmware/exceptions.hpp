/**
 * libaries
 * Copyright (C) 2021  aries-platform / libraries
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @file exceptions.hpp
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 *
 */
#ifndef EXCEPTIONS_HPP
#define EXCEPTIONS_HPP

#include <stdexcept>
#include <string>
// section:manual_includes
// endsection:manual_includes

namespace aries {

/**
 *
 */
class CrcError : public std::runtime_error
{
    // section:manual_definitions
    // endsection:manual_definitions

  public:
    /**
     *
     * @param[in] what
     */
    CrcError(const std::string& what);

  protected:
  private:
};

/**
 *
 */
class ProtocolError : public std::runtime_error
{
    // section:manual_definitions
    // endsection:manual_definitions

  public:
    /**
     *
     * @param[in] what
     */
    ProtocolError(const std::string& what);

  protected:
  private:
};

/**
 *
 */
class TimeoutError : public std::runtime_error
{
    // section:manual_definitions
    // endsection:manual_definitions

  public:
    /**
     *
     * @param[in] what
     */
    TimeoutError(const std::string& what);

  protected:
  private:
};

/**
 *
 */
class IOError : public std::runtime_error
{
    // section:manual_definitions
    // endsection:manual_definitions

  public:
    /**
     *
     * @param[in] what
     */
    IOError(const std::string& what);

  protected:
  private:
};

} // namespace aries

#endif /* EXCEPTIONS_HPP */