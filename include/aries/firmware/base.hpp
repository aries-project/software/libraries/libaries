/**
 * libaries
 * Copyright (C) 2021  aries-platform / libraries
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @file base.hpp
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 *
 */
#ifndef BASE_HPP
#define BASE_HPP

#include <cstdint>
#include <serialxx/serial.hpp>
#include <string>
#include <vector>
// section:manual_includes
// endsection:manual_includes

namespace aries {

namespace firmware {

    /**
     * Mesage response
     */
    struct MessageResponse
    {
        /// Sender address
        uint8_t address;
        /// Response payload
        std::vector<uint8_t> payload;
        /// Contains valid response
        bool valid;
    };

    /**
     *
     */
    struct FwVersion
    {
        /// Major version
        uint8_t major;
        /// Minor version
        uint8_t minor;
        /// Patch level
        uint8_t patch;
    };

    /**
     *
     */
    class Base
    {
        // section:manual_definitions
        // endsection:manual_definitions

      public:
        /**
         *
         * @param[in] serial
         */
        Base(serialxx::thread_safe::Serial& serial);

        /**
         * Send message to device on the databus.
         * @param[in] address Device address
         * @param[in] payload Message payload
         * @param[in] max_length Maximum response length
         * @return Message response
         */
        MessageResponse send(uint8_t address, const std::vector<uint8_t>& payload, unsigned max_length = 32);

        /**
         * Receive message from the databus.
         * @param[in] max_length Maximum number of bytes to receive. If set to zero maximum payload is expected (254)
         * @return Message response
         */
        MessageResponse recv(unsigned max_length = 32);

        /**
         * Send alive command and get status.
         * @param[in] address Device address
         * @return True if ALIVE response is received, False otherwise
         */
        bool getAlive(uint8_t address);

        /**
         *
         * @param[in] address
         * @return
         */
        FwVersion getFwVersion(uint8_t address);

        /**
         *
         * @param[in] address
         * @return
         */
        std::string getFwName(uint8_t address);

        /**
         * Set debug messages
         * @param[in] value
         */
        void setDebug(bool value);

        /**
         * Get debug messages
         * @return
         */
        bool getDebug() const;

      protected:
        /// Serial data interface
        serialxx::thread_safe::Serial& m_serial;
        /// Debug messages
        bool m_debug = false;
        /**
         * Write RAW payload to serial bus.
         * @param[in] address Device address
         * @param[in] payload Message payload
         * @return Number of bytes written
         */
        unsigned write(uint8_t address, const std::vector<uint8_t>& payload);

      private:
    };

} // namespace firmware

} // namespace aries

#endif /* BASE_HPP */