#ifndef ARIES_STD_HPP
#define ARIES_STD_HPP

#define ARIES_ADDR_FIELD_BYTES 1
#define ARIES_LENGTH_FIELD_BYTES 2
#define ARIES_CRC_FIELD_BYTES 1
#define ARIES_MIN_RESP_BYTES 1
#define ARIES_MAX_PAYLOAD_BYTES (255 - ARIES_CRC_FIELD_BYTES)

#define ARIES_MSG_TIMEOUT_MS 200

#define ARIES_ADDR_MASK 0x7f
#define ARIES_ADDR_RESP 0x40
#define ARIES_BROADCAST 0xff
#define ARIES_UNINITIALIZED 0x80

#define ARIES_UCAST(addr) (addr & ARIES_ADDR_MASK)
#define ARIES_IS_BCAST(addr) (addr == ARIES_BROADCAST)
#define ARIES_IS_RESP(addr) (addr & ARIES_ADDR_RESP)

#define CMD(tag, id) ((((tag - 'A' + 1) << 3) & 0xf8) | (id & 0x07))

#define RESP_OK 0x10
#define RESP_FAILED 0x11
#define RESP_UNKNOWN 0x12

#define CMD_A0 CMD('A', 0)
#define CMD_A1 CMD('A', 1)
#define CMD_A2 CMD('A', 2)
#define CMD_A3 CMD('A', 3)
#define CMD_A4 CMD('A', 4)
#define CMD_A5 CMD('A', 5)
#define CMD_A6 CMD('A', 6)
#define CMD_A7 CMD('A', 7)

#define CMD_B0 CMD('B', 0)
#define CMD_B1 CMD('B', 1)
#define CMD_B2 CMD('B', 2)
#define CMD_B3 CMD('B', 3)
#define CMD_B4 CMD('B', 4)
#define CMD_B5 CMD('B', 5)
#define CMD_B6 CMD('B', 6)
#define CMD_B7 CMD('B', 7)

#define CMD_C0 CMD('C', 0)
#define CMD_C1 CMD('C', 1)
#define CMD_C2 CMD('C', 2)
#define CMD_C3 CMD('C', 3)
#define CMD_C4 CMD('C', 4)
#define CMD_C5 CMD('C', 5)
#define CMD_C6 CMD('C', 6)
#define CMD_C7 CMD('C', 7)

#define CMD_D0 CMD('D', 0)
#define CMD_D1 CMD('D', 1)
#define CMD_D2 CMD('D', 2)
#define CMD_D3 CMD('D', 3)
#define CMD_D4 CMD('D', 4)
#define CMD_D5 CMD('D', 5)
#define CMD_D6 CMD('D', 6)
#define CMD_D7 CMD('D', 7)

#define CMD_E0 CMD('E', 0)
#define CMD_E1 CMD('E', 1)
#define CMD_E2 CMD('E', 2)
#define CMD_E3 CMD('E', 3)
#define CMD_E4 CMD('E', 4)
#define CMD_E5 CMD('E', 5)
#define CMD_E6 CMD('E', 6)
#define CMD_E7 CMD('E', 7)

#define CMD_F0 CMD('F', 0)
#define CMD_F1 CMD('F', 1)
#define CMD_F2 CMD('F', 2)
#define CMD_F3 CMD('F', 3)
#define CMD_F4 CMD('F', 4)
#define CMD_F5 CMD('F', 5)
#define CMD_F6 CMD('F', 6)
#define CMD_F7 CMD('F', 7)

#define CMD_G0 CMD('G', 0)
#define CMD_G1 CMD('G', 1)
#define CMD_G2 CMD('G', 2)
#define CMD_G3 CMD('G', 3)
#define CMD_G4 CMD('G', 4)
#define CMD_G5 CMD('G', 5)
#define CMD_G6 CMD('G', 6)
#define CMD_G7 CMD('G', 7)

#define CMD_H0 CMD('H', 0)
#define CMD_H1 CMD('H', 1)
#define CMD_H2 CMD('H', 2)
#define CMD_H3 CMD('H', 3)
#define CMD_H4 CMD('H', 4)
#define CMD_H5 CMD('H', 5)
#define CMD_H6 CMD('H', 6)
#define CMD_H7 CMD('H', 7)

#define CMD_I0 CMD('I', 0)
#define CMD_I1 CMD('I', 1)
#define CMD_I2 CMD('I', 2)
#define CMD_I3 CMD('I', 3)
#define CMD_I4 CMD('I', 4)
#define CMD_I5 CMD('I', 5)
#define CMD_I6 CMD('I', 6)
#define CMD_I7 CMD('I', 7)

#define CMD_J0 CMD('J', 0)
#define CMD_J1 CMD('J', 1)
#define CMD_J2 CMD('J', 2)
#define CMD_J3 CMD('J', 3)
#define CMD_J4 CMD('J', 4)
#define CMD_J5 CMD('J', 5)
#define CMD_J6 CMD('J', 6)
#define CMD_J7 CMD('J', 7)

#define CMD_K0 CMD('K', 0)
#define CMD_K1 CMD('K', 1)
#define CMD_K2 CMD('K', 2)
#define CMD_K3 CMD('K', 3)
#define CMD_K4 CMD('K', 4)
#define CMD_K5 CMD('K', 5)
#define CMD_K6 CMD('K', 6)
#define CMD_K7 CMD('K', 7)

#define CMD_L0 CMD('L', 0)
#define CMD_L1 CMD('L', 1)
#define CMD_L2 CMD('L', 2)
#define CMD_L3 CMD('L', 3)
#define CMD_L4 CMD('L', 4)
#define CMD_L5 CMD('L', 5)
#define CMD_L6 CMD('L', 6)
#define CMD_L7 CMD('L', 7)

#define CMD_M0 CMD('N', 0)
#define CMD_M1 CMD('N', 1)
#define CMD_M2 CMD('N', 2)
#define CMD_M3 CMD('N', 3)
#define CMD_M4 CMD('N', 4)
#define CMD_M5 CMD('N', 5)
#define CMD_M6 CMD('N', 6)
#define CMD_M7 CMD('N', 7)

#define CMD_R0 CMD('R', 0)
#define CMD_R1 CMD('R', 1)
#define CMD_R2 CMD('R', 2)
#define CMD_R3 CMD('R', 3)
#define CMD_R4 CMD('R', 4)
#define CMD_R5 CMD('R', 5)
#define CMD_R6 CMD('R', 6)
#define CMD_R7 CMD('R', 7)

#define CMD_S0 CMD('S', 0)
#define CMD_S1 CMD('S', 1)
#define CMD_S2 CMD('S', 2)
#define CMD_S3 CMD('S', 3)
#define CMD_S4 CMD('S', 4)
#define CMD_S5 CMD('S', 5)
#define CMD_S6 CMD('S', 6)
#define CMD_S7 CMD('S', 7)

#define CMD_V0 CMD('V', 0)
#define CMD_V1 CMD('V', 1)
#define CMD_V2 CMD('V', 2)
#define CMD_V3 CMD('V', 3)
#define CMD_V4 CMD('V', 4)
#define CMD_V5 CMD('V', 5)
#define CMD_V6 CMD('V', 6)
#define CMD_V7 CMD('V', 7)

#define CMD_W0 CMD('W', 0)
#define CMD_W1 CMD('W', 1)
#define CMD_W2 CMD('W', 2)
#define CMD_W3 CMD('W', 3)
#define CMD_W4 CMD('W', 4)
#define CMD_W5 CMD('W', 5)
#define CMD_W6 CMD('W', 6)
#define CMD_W7 CMD('W', 7)

#endif /* ARIES_STD_HPP */