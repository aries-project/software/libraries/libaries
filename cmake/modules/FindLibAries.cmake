# This module tries to find libserialxx library and include files
#
# LIBARIES_INCLUDE_DIR, path where to find libwebsockets.h
# LIBARIES_LIBRARY_DIR, path where to find libwebsockets.so
# LIBARIES_LIBRARIES, the library to link against
# LIBARIES_FOUND, If false, do not try to use libWebSockets
#
# This currently works probably only for Linux

FIND_PATH ( LIBARIES_INCLUDE_DIR aries/aries.hpp
    /usr/local/include
    /usr/include
)

FIND_LIBRARY ( LIBARIES_LIBRARIES aries
    /usr/local/lib
    /usr/lib
)

GET_FILENAME_COMPONENT( LIBARIES_LIBRARY_DIR ${LIBARIES_LIBRARIES} PATH )

SET ( LIBARIES_FOUND "NO" )
IF ( LIBARIES_INCLUDE_DIR )
    IF ( LIBARIES_LIBRARIES )
        SET ( LIBARIES_FOUND "YES" )
    ENDIF ( LIBARIES_LIBRARIES )
ENDIF ( LIBARIES_INCLUDE_DIR )

MARK_AS_ADVANCED(
    LIBARIES_LIBRARY_DIR
    LIBARIES_INCLUDE_DIR
    LIBARIES_LIBRARIES
)
